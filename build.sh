#!/usr/bin/env bash
drush make drupal.make.yml web
drush --root=web si -y \
  --db-url=mysql://drupal:drupal@mariadb/drupal \
  --account-name=admin \
  --account-pass=1 \
  --account-mail=admin@example.com \
  --site-mail=admin@example.com \
  --site-name=Drupal \
  standard
