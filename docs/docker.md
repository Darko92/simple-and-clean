# Docker, containers virtualization

## Why `docker`

There are many problems with local setup for development, some of them are

1. Setting everything right
2. Upgrading OS may break some hand made tuning
3. Difference with stage/production like PHP version, services like `solr`
4. Using some specific setup for a project
5. Differences with developers' computers like different platforms, OS-es, versions
6. ...and many more potential pitfalls

Docker is a lightweight virtualization comparing to full virtualization like Virtual Box, VMWare, Parallels or similar.

There are some differences between implementation on **Linux** and on **Docker for Mac** or **Docker for Windows**.

## Requirements for `docker`

Most of the systems today are capable of running `docker` on Linux, however on Mac and Windows there are additional requirements:

* **All platforms** — 2GB at least, however for some comfort 8GB on system is recommended
* **Linux** — any 64-bit x86 compatible processor with EPT instruction set and kernel newer than 3.10
* **Mac** — 4GB of memory, Mac OS X 10.10.3 (Yosemite) or newer, Intel i3, i5, i7 or Xeon processor (machines newer than 2010)
* **Windows** — any 64-bit x86 compatible processor with EPT instruction set and 64bit Windows 10 Pro, Enterprise and Education (1511 November update, Build 10586 or later). with Hypervisor enabled

**Linux** based workstation should be updated to latest stable OS version for optimal development experience. Installation details can be found in [Installing Docker](install_docker.md) document and on [docker site](http://docker.com/). You should also consider using Linux kernel 4.4 or newer and adjust kernel boot arguments. You can find particular information [here](https://docs.docker.com/engine/installation/linux/)

**Macs** use native **Docker for Mac**. In case you wish to use `docker` on Macs older than 2010, you can use older `docker toolbox`, details are [here](https://docs.docker.com/engine/installation/mac/)

**Windows** can also use native solution **Docker for Windows** under Windows 10, otherwise also ahve to use `docker toolbox`, details are [here](https://docs.docker.com/engine/installation/windows/)

## Docker4Drupal

[Docker4Drupal](http://docker4drupal.org/) project is just one of many existing Drupal stacks. It was chosen due to small size and easy usage scenario. There are many options that could be set in configuration files on per project basis.

## `docker` commands

`docker` controls individual containers by name or ID while `docker-compose` controls a group of containers, usually called _application_ or _stack_.

Usual Docker commands:

- `docker ps -a` — lists all docker containers, running, paused and stopped.
- `docker inspect mcidev_nginx_1` — lists all details about the container by name or ID
- `docker stats --no-stream` — shows resource usage, omit `--no-stream` to have live stats

## `docker-compose` commands

- `docker-compose up -d` — while in directory structure containing `docker-compose.yml`, creates and starts application.
- `docker-compose pull` — fetch latest version of containers.
- `docker-compose stop` — stops application
- `docker-compose start` — starts application
- `docker-compose restart` — restarts application
- `docker-compose down` — remove containers (instance of contaners images)

