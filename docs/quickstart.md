# Quick new project start

Docker is constantly and rapidly changing. So are containers as their creators learn more and try to use the technology to the full potential. Wodby containers changed both as updated and using new environment variables so old configuration files (without version) are not working anymore as defined in previous version of **MCI-Drupal** template.

## Starting a new project `newproj`

### Create a new directory

Create a new directory where you usually hold project, presumably /home/_your_user_/Sites

    ~/Sites $ mkdir newproj

### Download docker configuration

Download included mci.docker-compose.yml, rename it and copy to project directory

    ~/Sites $ cp ~/Downloads/mci.docker-compose.yml ~/Sites/newproj/docker-compose-yml

### Open directory in PhpStorm

In PhpStorm use File -> Open navigate to your project and just select the directory.

Take into account that sometimes PhpStorm does not refresh files listing so you have to toggle to make it refresh.

### Open docker-compose.yml and change

Open the file in PhpStorm and change all occurences of mci with newproj (your project name). This is tuned for Drupal 8, in case you need Drupal 7, change:

1. PHP to 5.6, just comment out current line with php:7.0 image and uncomment next one with 5.6
2. Change in nginx DRUPAL-VERSION to 7 (that switches proper request handling)

### Start containers

Just use plain, standard

    ~/Sites/newproj/ $ docker-compose up -d

If it complains that it does not have traefik_proxy network, just create one

    ~/Sites/newproj $ docker-compose up -d
    Creating network "newproj_default" with the default driver
    ERROR: Network traefik_proxy declared as external, but could not be found. Please create the network manually using `docker network create traefik_proxy` and try again.

    ~/Sites/newproj $ docker network create traefik_proxy
    e0a609a5e6f8c03a87cc656dcb628eead218c58821f46e44462c1772302bd158

    ~/Sites/newproj $ docker-compose up -d
    Creating volume "newproj_codebase" with default driver
    Pulling nginx (wodby/drupal-nginx:1.10)...
    1.10: Pulling from wodby/drupal-nginx
    0a8490d0dfd3: Already exists
    744357536647: Pull complete
    dbe3895affaa: Pull complete
    09794d404ac3: Pull complete
    3887894360a9: Pull complete
    f2fbfd5c33e1: Pull complete
    997631e73dc5: Pull complete
    Digest: sha256:c549725a691c2469c522c65f973e8f91496f3512a78ad1bc6e9ea8fb5ea63649
    Status: Downloaded newer image for wodby/drupal-nginx:1.10
    Creating newproj_mailhog_1
    Creating newproj_php_1
    Creating newproj_mariadb_1
    Creating newproj_pma_1
    Creating newproj_nginx_1

### Get new Drupal code

To do that, you have to enter PHP container to use drush, this is the simplest way. By default, drush fetches the latest stable Drupal version, now it is Drupal 8.3.2 (in case you need Drupal 7 use drush dl drupal-7)

    ~/Sites/newproj/ $ docker-compose php exec sh
    /var/www/html # drush dl drupal 
    Project drupal (8.3.2) downloaded to /var/www/html/drupal-8.3.2. [success]
    Project drupal contains:                                         [success]
     - 1 profile: standard
     - 14 themes: testing_config_import, minimal, testing_multilingual_with_english, testing_multilingual, drupal_system_listing_compatible_test, testing, testing_config_overrides, testing_missing_dependencies,
    classy, bartik, stark, stable, twig, seven
     - 73 modules: action, basic_auth, quickedit, contact, field_ui, migrate, syslog, ckeditor, datetime, node, history, block_content, text, workflows, ban, comment, editor, migrate_drupal, options, telephone,
    contextual, shortcut, automated_cron, image, language, menu_ui, entity_reference, taxonomy, dblog, config, breakpoint, menu_link_content, views_ui, content_moderation, help, tracker, block, content_translation,
    rest, config_translation, system, outside_in, file, big_pipe, page_cache, tour, rdf, hal, dynamic_page_cache, views, field, toolbar, book, block_place, search, user, path, field_layout, update,
    migrate_drupal_ui, forum, inline_form_errors, responsive_image, simpletest, aggregator, link, statistics, locale, filter, datetime_range, layout_discovery, serialization, color
    /var/www/html # ll
    total 32
    drwxrwxr-x 6 user user 4096 мај 16 10:02 ./
    drwxrwxr-x 5 user user 4096 мај 16 09:49 ../
    drwxr-xr-x 2 root root 4096 мај 16 10:00 databases/
    -rw-rw-r-- 1 user user 4594 мај 16 09:58 docker-compose.yml
    drwxr-xr-x 8 root root 4096 мај  3 20:04 drupal-8.3.2/
    drwxrwxr-x 2 user user 4096 мај 16 09:58 .idea/
    drwxr-xr-x 5 root root 4096 мај 16 10:00 runtime/

### Rename drupal-8.x.x to webroot

While still in container, rename document root directory. There were many doc-something so tab expansion was not working, so I changed it to be rooted at `webroot`

    /var/www/html # mv drupal-8.3.2/ webroot
    /var/www/html # exit
    ~/Sites/newproj $ ll
    total 32
    drwxrwxr-x 6 user user 4096 мај 16 10:02 ./
    drwxrwxr-x 5 user user 4096 мај 16 09:49 ../
    drwxr-xr-x 2 root root 4096 мај 16 10:00 databases/
    -rw-rw-r-- 1 user user 4594 мај 16 09:58 docker-compose.yml
    drwxr-xr-x 8 root root 4096 мај  3 20:04 drupal-8.3.2/
    drwxrwxr-x 2 user user 4096 мај 16 10:03 .idea/
    drwxr-xr-x 5 root root 4096 мај 16 10:00 runtime/

### Fix ownership

In order for both you and docker to have access to files, do the following:

    ~/Sites/newproj $ sudo chown -R USER:docker webroot
    [sudo] password for USER:
    ~/Sites/newproj $ chmod -R g+w webroot/
    ~/Sites/newproj $ ll
    total 32
    drwxrwxr-x 6 user user 4096 мај 16 10:05 ./
    drwxrwxr-x 5 user user 4096 мај 16 09:49 ../
    drwxr-xr-x 2 root root 4096 мај 16 10:00 databases/
    -rw-rw-r-- 1 user user 4594 мај 16 09:58 docker-compose.yml
    drwxrwxr-x 2 user user 4096 мај 16 10:05 .idea/
    drwxr-xr-x 5 root root 4096 мај 16 10:00 runtime/
    drwxrwxr-x 8 user docker       4096 мај  3 20:04 webroot/

### You are now ready to test in browser

Try accessing your domains in browser (do not forget trailing slash so that Google does not search for your local domain)

1. http://newproj.dev.loc is for the site
2. http://newproj.pma.loc is for the site
3. http://newproj.hog.loc is for the site

### Database credentials

Once you open site in browser, installation will start.
Don't forget that containers have separated services so there is no localhost for database, it is like it runs on different server and the name is mariadb

database: drupal
user: drupal
password: drupal
host: mariadb

### Further drush and drupal console operations

The best way is to have a terminal open in PHP container all the time, working directory must be webroot

    ~/Sites/newproj/ $ docker-compose php exec sh
    /var/www/html # cd webroot
    /var/www/html/webroot # drush st

### Useful drush commands

Clear cache is now cache rebuild. As many things are now cached, you have to do this often to avoid strange situations

    /var/www/html/webroot # drush cr

Download additional module just like always

    /var/www/html/webroot # drush dl panels

The best way to operate the database is directly through drush/mysql.

Make database dump

    /var/www/html/webroot # drush sql-dump > ../databases/newproj-2017-05-16_1632.sql

Drop all tables from database (be aware that is what you want!!!)

    /var/www/html/webroot # drush sql-drop
    Do you really want to drop all tables in the database drupal? (y/n): 

Connect to database to import

    /var/www/html/webroot # drush  sqlc
    Welcome to the MariaDB monitor.  Commands end with ; or \g.
    Your MariaDB connection id is 12
    Server version: 10.1.21-MariaDB MariaDB Server
    
    Copyright (c) 2000, 2016, Oracle, MariaDB Corporation Ab and others.
    
    Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
    
    MariaDB [drupal]> source /var/www/html/databases/newproj-2017-05-16_1632.sql
    ...
    ...
    Query OK, 0 rows affected (0.00 sec)
    
    Query OK, 0 rows affected (0.00 sec)
    
    MariaDB [drupal]> exit
    Bye
    /var/www/html # exit
    ~/Sites/new

### Useful `docker` and `docker-compose` commands

See [here](/docs/docker.md) more about installing docker and [commands](/docs/docker.md#) 
